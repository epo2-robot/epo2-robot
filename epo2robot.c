#include "epo2robot.h"

/*

High-level functies

*/

/*
*Vervanger voor traceroute()
- Genereert route
*/
void generateroute(Location source, Location destination, int startIsBackwards)
{
    Location previous, now, next;

    printf("[*] Generating route\n");
    printf("\tsource.x: %d, source.y:%d, destination.x:%d, destination.y:%d\n",
        source.x, source.y, destination.x, destination.y);

    /* Maak map leeg */
    clearmap(maze);

    /* Maak oude route list leeg */
    free_route(first_path);

    /* Vraag nieuw geheugenblok aan besturingssysteem en zet dit in route pointer */
    route = safe_malloc(sizeof (route_path));
    /* first_path  bevat adres van eerste route. route pointer verschuift naarmate verder in route */
    first_path = route;

    printf("First value of pointer: %p\n", (void*)route);

    /*
    Lee algoritme
    - source heeft hoogste getal
    */
    printf("\t[*] Leefilling\n");
        leefill(source, destination);
    printf("\t\tDone!\n");

    /* Stel waardes in eerste route element */
    route->loc = source;
    route->direction_to_next = CMD_FORWARD;
    route->next = NULL;

    /*
    previous, coordinaat van vorige locatie
    now, locatie waar robot nu is
    next, volgende locatie
    */
    if (startIsBackwards)
    {
        previous = previous_location[0];
        now = source;
    }
    else
    {
        previous = source;
        now = findlowest(source);

    }

    printf("previous x:%d, y:%d\n", previous.x, previous.y);

    printf("now x:%d, y:%d\n", now.x, now.y );

    printf("\t[*]Tracebacking...\n");

    print_maze(maze, PRINT_VALUES);

    while (1)
    {
        next = findlowest(now);
        /* findLowest zet coordinaat op -1 als er geen lager is. */
            if ( next.x < 0 )
                break;

        /* Forwards */
            /* "Boven" */
            if ( next.y < now.y && now.y < previous.y )
                append_path(route, now, CMD_FORWARD);
            /* "Onder" */
            else if ( next.y > now.y &&  now.y > previous.y )
                append_path(route, now, CMD_FORWARD);

            /* "Links" */
            else if ( next.x < now.x && now.x < previous.x )
                append_path(route, now, CMD_FORWARD);
            /* "Rechts" */
            else if ( next.x > now.x &&  now.x > previous.x )
                append_path(route, now, CMD_FORWARD);

        /* Linksen */
            /* "Boven" */
            else if ( next.x < now.x && now.y < previous.y )
                append_path(route, now, CMD_LEFT);
            /* "Onder" */
            else if ( next.x > now.x && now.y > previous.y )
                append_path(route, now, CMD_LEFT);

            /* "Links" */
            else if ( next.y > now.y && now.x < previous.x )
                append_path(route, now, CMD_LEFT);
            /* "Rechts" */
            else if ( next.y < now.y && now.x > previous.x )
                append_path(route, now, CMD_LEFT);

        /* Rechtsen */
            /* "Boven" */
            else if ( next.x > now.x && now.y < previous.y )
                append_path(route, now, CMD_RIGHT);
            /* "Onder" */
            else if ( next.x < now.x && now.y > previous.y )
                append_path(route, now, CMD_RIGHT);

            /* "Links" */
            else if ( next.y < now.y && now.x < previous.x )
                append_path(route, now, CMD_RIGHT);
            /* "Rechts" */
            else if ( next.y > now.y && now.x > previous.x )
                append_path(route, now, CMD_RIGHT);

        /* TODO: Backwards */

            else
            {
                printf("[-] ERROR: Unknown direction! Assuming forward\n");
                append_path(route, now, CMD_FORWARD);
            }

            previous = now;
            now = next;
    }

    /*
    Wordt gebruikt nadat mijn is gevonden. Robot komt namelijk achteruit terug als hij mijn heeft gevonden.
    - Dit houdt eigenlijk in dat alle richtingen worden omgedraait
    */
    if (startIsBackwards)
    {
        advance();

        if (route->direction_to_next == CMD_FORWARD)
            route->direction_to_next = CMD_U_TURN;
        else if (route->direction_to_next == CMD_LEFT)
            route->direction_to_next = CMD_RIGHT;
        else if (route->direction_to_next == CMD_RIGHT)
            route->direction_to_next = CMD_LEFT;
        else
            printf("generateroute() error. startIsBackwards\n");

        printf("Made the backwards direction: %d\n", route->direction_to_next);
    }
}

/* Voegt een nieuw pad toe aan route */
void append_path(route_path *route, Location loc, int direction)
{
    route_path *rptmp = route;
    route_path *path_to_be_appended = safe_malloc(sizeof (route_path));

    printf("Appending... %p to ", (void*)path_to_be_appended);

    /* Ga naar eind van linked list */
    while (rptmp->next)
        rptmp = rptmp->next;

    printf("%p\n", rptmp);

    /* Instellen van path_to_be_appended */
    path_to_be_appended->loc = loc;
    path_to_be_appended->direction_to_next = direction;
    path_to_be_appended->next = NULL;

    /* Toevoegen aan einde */
    rptmp->next = path_to_be_appended;
}

/* Geef geheugen vrij van linked list */
void free_route(route_path *route)
{
    route_path *rptmp;

    while (route)
    {
        rptmp = route;
        route = route->next;

        if (rptmp != NULL)
        {
            free(rptmp);
        }
    }
}


/* Print route */
void print_route(route_path *route)
{
    route_path *rptmp;

    printf("[*] Printing route:\n");

    /* rptmp = route->next omdat eerste route 0 is */
    for ( rptmp = route; rptmp != NULL; rptmp = rptmp->next )
    {
        static char direction_text[10];

        if (rptmp->direction_to_next == CMD_FORWARD)
            sprintf(direction_text, "FORWARD");
        if (rptmp->direction_to_next == CMD_LEFT)
            sprintf(direction_text, "LEFT");
        if (rptmp->direction_to_next == CMD_RIGHT)
            sprintf(direction_text, "RIGHT");
        if (rptmp->direction_to_next == CMD_START)
            sprintf(direction_text, "Begin");

        printf("\taddr:%p x:%d y:%d dir:%s next_addr:%p\n", (void *)rptmp, rptmp->loc.x, rptmp->loc.y, direction_text, (void *)rptmp->next);
    }
}

/* Verplaats route pointer een verder */
void advance()
{
    previous_location[1] = previous_location[0];
    previous_location[0] = route->loc;
    route = route->next;
}


/*

MAIN

*/
int main()
{
/* Vul map met leeg veld en wegen */
    initmap(maze);

    printf("[*] Parameters: \n");
/* Lees geblokkeerde wegen */
    printf("\t# Blocked roads: ");
    scanf("%d", &i);
    for (j = 0; j < i; j++)
    {
        printf("\t\tRoad %d: ", j);
        scanf("%s", roadstring);
        placeroadblock( roadnametolocation(roadstring) );
    }

    /* print maze with blockades */
    #ifdef DEBUG
        print_maze(maze, PRINT_VALUES);
        printf("---------------------------------------------------------------\n");
    #endif

/* Lees beginpunt */
    printf("\tBeginpunt: ");
    scanf("%d", &j);
    source = findpunt(j);

/* Lees checkpoints */
    printf("\t# of destinations: ");
    scanf("%d", &numberOfCheckpoints);
    printf("\t\tDestination names: ");
    for (i = 0; i < numberOfCheckpoints; i++)
    {
        scanf("%d", &j);
        checkpoint[i] = findpunt(j);
        printf("Checkpoint x:%d y:%d\n", checkpoint[i].x, checkpoint[i].y);
    }
    currentCheckpoint = -1;
    destinationCheckpoint = 0;

    /* Genereer route */
    generateroute(source, checkpoint[destinationCheckpoint], 0);

    print_route(route);

    /* Instelle serieel */
    serial_init();

    /* Eerste commando */
    serial_write_byte(CMD_START);
    advance();

    printf("Waiting for NEXT_CMD_REQUEST\n");

    /* Terwijl nog checkpoints zijn */
    while (1)
    {
        /* Wachten totdat robot een terugsignaal stuurt, dan nieuw commando sturen */
        while (1)
        {
            serial_read_byte(&received);

            if (received == NEXT_CMD_REQUEST)
            {
                printf("[*] Received NEXT_CMD_REQUEST\n");
                serial_write_byte(route->direction_to_next);
                advance();

                /* received buffer leegmaken */
                received = 0;

                /* Als eind van chain */
                if ( route == NULL )
                    break;
            }
            else if (received == MINEFOUND)
            {
                printf("Received MINEFOUND\n");

                placeroadblock(previous_location[0]);
                generateroute(previous_location[1], checkpoint[destinationCheckpoint], 1);

                print_route(route);

                serial_write_byte(route->direction_to_next);
                advance();

                received = 0;
            }
            else if (received == NACK)
            {
                serial_write_byte(lastsentcommand);
                printf("Received NACK resending command: 0x%02X\n", lastsentcommand);
            }
            else if (received == 0)
            {
                ; /* Geen commando, wordt gecalle als seriele bus in non-blocking mode seriela_read_byte called */
            }
            else
            {
               printf("Received unknown command: 0x%02X\n", received);
               received = 0;
            }
        }

        /* If arrived at final destination */
        if (destinationCheckpoint == (numberOfCheckpoints - 1))
            break;

        /* Otherwise proceed to next checkpoint */
        currentCheckpoint++;
        destinationCheckpoint++;
        printf("[+] Navigating from currentCheckpoint %d, to destinationCheckpoint %d (Total: %d)\n", currentCheckpoint, destinationCheckpoint, numberOfCheckpoints);

        generateroute(checkpoint[currentCheckpoint], checkpoint[destinationCheckpoint], 0);
        advance();

        print_route(route);
    }

    printf("Waiting for final NEXT_CMD_REQUEST\n");
    while (received != NEXT_CMD_REQUEST)
        serial_read_byte(&received);

    /* stop */
    serial_write_byte(CMD_HALT);

    /* Sluiten van seriele verbinding */
    serial_close();
    /* Freeen van route linked list memory */
    free_route(route);

    printf("Program came to end of execution!\n");

    return 0;
}

