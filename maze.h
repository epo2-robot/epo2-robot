#ifndef MAZE_H
#define MAZE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct cell Cell;

struct cell
{
    int v;
    char name[8];
};


typedef struct location Location;

struct location
{
    int x;
    int y;
};

Cell maze[13][13];


/* Map vullen met -1en en 0en */
void initmap(Cell maze[13][13]);

/* Vervangt alle waardes die >= 0 in een map met 0 */
void clearmap(Cell maze[13][13]);

#define PRINT_VALUES 0
#define PRINT_NAMES 1
void print_maze(Cell maze[13][13], int print_mode);

/* Doet +1 bij alle vakken om zich heen */
void pluseen(int c, int i, int j);

/* Returnt het laagst aanliggende coordinaat */
Location findlowest(Location current);

/* Maakt van een roadnaam een coordinaat in het veld */
/* bv e2030 -> 0,6 : 0,8 */
Location roadnametolocation(char roadstring[8]);

/* Maakt van een knooppuntnaam een location */
/* bv c01 naar (4,2) */
Location knooppunttolocation(char string[8]);

/* Vind coordinaat van een punt met gegeven naam [1-12]*/
Location findpunt(int puntnaam);

/* Veld invullen met Lee algoritme */
void leefill(Location source, Location destination);

/* Plaats een -1 op gegeven locatie */
void placeroadblock(Location road);


#endif /* MAZE_H */