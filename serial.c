/*

Seriele communicatie

*/

#include "serial.h"


/*
Linux heeft andere code voor seriele communicatie.
*/
#ifdef __linux__
	#include <stdlib.h>
	#include <unistd.h> 
	#include <fcntl.h>
	#include <errno.h>
	#include <termios.h>

	int fd;
	char *portname = "/dev/ttyUSB1";

	void serial_init()
	{
		;
	}

	void serial_write_byte(char bytetowrite)
	{
		lastsentcommand = bytetowrite;

		/*printf("[*] Simulated WRITE byte: 0x%02x %c\n", bytetowrite, bytetowrite);*/
		printf("Writing byte: %02x\n", bytetowrite);
		write(fd, &bytetowrite, 1);
	}

	void serial_read_byte(char *buffRead)
	{
		/*printf("[*] Simulated READ byte: k\n");*/
		buffRead[0] = read(fd, buffRead, 1);
		printf("Read: %02X\n", *buffRead);
	}

	void serial_close()
	{
		;
	}


/*Op windows*/
#else
    #include <stdlib.h>
    #include <Windows.h>
    #include <string.h>

	#define COMPORT "COM3"
	#define BAUDRATE CBR_9600

	HANDLE hSerial;
	char byteBuffer[BUFSIZ+1];

	void serial_init()
	{
		printf("[+] Initializing...\n");

	    hSerial = CreateFile(COMPORT,
	        GENERIC_READ | GENERIC_WRITE,
	        0,
	        0,
	        OPEN_EXISTING,
	        FILE_ATTRIBUTE_NORMAL,
	        0
	    );

	    if(hSerial == INVALID_HANDLE_VALUE){
	        if(GetLastError()== ERROR_FILE_NOT_FOUND){

	            printf("\tserial port does not exist \n");
	        }

	        printf("\tsome other error occured. Inform user.\n");
	    }


	    COMMTIMEOUTS timeouts = {0};
	    DCB dcbSerialParams = {0};

	    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

	    if (!GetCommState(hSerial, &dcbSerialParams)) {

	        printf("\terror getting state \n");
	    }

	    dcbSerialParams.BaudRate = BAUDRATE;
	    dcbSerialParams.ByteSize = 8;
	    dcbSerialParams.StopBits = ONESTOPBIT;
	    dcbSerialParams.Parity   = NOPARITY;

	    if(!SetCommState(hSerial, &dcbSerialParams)){

	        printf("\terror setting state \n");
	    }

	    timeouts.ReadIntervalTimeout = 50;
	    timeouts.ReadTotalTimeoutConstant = 50;
	    timeouts.ReadTotalTimeoutMultiplier = 10;

	    timeouts.WriteTotalTimeoutConstant = 50;
	    timeouts.WriteTotalTimeoutMultiplier = 10;

	    if(!SetCommTimeouts(hSerial, &timeouts)){

	        printf("\terror setting timeout state \n");
	    }

	    printf("\tDone!\n");
	}

	void serial_write_byte(char bytetowrite)
	{
	    DWORD dwBytesWritten = 0;

	    lastsentcommand = bytetowrite;

	    char *buffWrite = &bytetowrite;

	    if (!WriteFile(hSerial, buffWrite, 1, &dwBytesWritten, NULL))
	    {
	        printf("error writing byte to output buffer \n");
	    }
	    printf("Byte written to write buffer is: c:%c 0x%02X \n", buffWrite[0], buffWrite[0]);

	    return(0);
	}

	void serial_read_byte(char *buffRead)
	{
	    DWORD dwBytesRead = 0;

	    if (!ReadFile(hSerial, buffRead, 1, &dwBytesRead, NULL))
	    {
	       /* printf("error reading byte from input buffer \n") */;
	    }
	   /* printf("Byte read from read buffer is: %c \n", buffRead[0]); */
	}

	void serial_close()
	{
		CloseHandle(hSerial);
	}

#endif /* OS_UNIX */
