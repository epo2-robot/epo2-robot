#ifndef EPO2ROBOT_H
#define EPO2ROBOT_H

#include <stdio.h>
#include <string.h>

/*sleep()*/
#include <unistd.h>

/* ctrl-c capture */
#include <signal.h>

#include "maze.h"
#include "serial.h"
#include "salloc.h"

/* For debug time library */
#include <time.h>
#include <stdint.h>

typedef struct Route_Path route_path;

#define LEFT 1
#define RIGHT 2
#define FORWARD 3
struct Route_Path
{
    Location loc;

    int direction_to_next;

    route_path *next;
};

/* Linked list van de route */
route_path *route;

/* Location of previous, used for minefound */
Location previous_location[2];

/* Element dat bijhoudt wt eerste pad is. Nodig om geheugen weer vrij te geven */
route_path *first_path;

/* 
Location structs. 
- detination geeft eindpunt aan van hele traject
- now geeft weer waar de robot is of naartoe aan het gaan is
- previous geeft locatie weer van plek waar we net vandaan kwamen
 */
Location source;

/* Intermediate stops */
Location checkpoint[10];
int currentCheckpoint, destinationCheckpoint;
int numberOfCheckpoints;

/* buffers voor input en iteration variabelen */
char roadstring[8];
int i, j;

/* Serial */
char received;



/* Debug timer */
clock_t t1, t2;


/* Maak een linked list met een route */
void generateroute(Location source, Location destination, 
	int startIsBackwards);

/* Voegt een nieuw pad toe aan route */
void append_path(route_path *route, Location loc, int direction);

/* Free'd memory van route linked list */
void free_route();

/* Print route */
void print_route(route_path *route);

/* Verplaatst route pointer naar volgende route */
void advance();

#endif /* EPO2ROBOT_H */