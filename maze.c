#include "maze.h"


void initmap(Cell maze[13][13])
{
    int i,j;
    for (i = 0; i < 13; i++)
    {
        for (j = 0; j < 13; j++)
        {
            /* Hekje */
            if (i == 4 || i == 6 || i == 8) maze[i][j].v = 0;
            else if (j == 4 || j == 6 || j == 8) maze[i][j].v = 0;
            /* Vierkant */
            else if (i == 2 && j >= 2 && j <= 10) maze[i][j].v = 0;
            else if (i == 10 && j >= 2 && j <= 10) maze[i][j].v = 0;
            else if (j == 2 && i >= 2 && i <= 10) maze[i][j].v = 0;
            else if (j == 10 && i >= 2 && i <= 10) maze[i][j].v = 0;
            /* -1 */
            else maze[i][j].v = -1;
        }
    }

    /* Namen van wegen, eindpunten en kruispunten */
    strcpy(maze[0][4].name,"9");
    strcpy(maze[0][6].name,"8");
    strcpy(maze[0][8].name,"7");
    strcpy(maze[4][0].name,"10");
    strcpy(maze[6][0].name,"11");
    strcpy(maze[8][0].name,"12");
    strcpy(maze[12][4].name,"1");
    strcpy(maze[12][6].name,"2");
    strcpy(maze[12][8].name,"3");
    strcpy(maze[4][12].name,"6");
    strcpy(maze[6][12].name,"5");
    strcpy(maze[8][12].name,"4");
    strcpy(maze[4][2].name,"c10");
    strcpy(maze[4][3].name,"e1011");
    strcpy(maze[4][4].name,"c11");
    strcpy(maze[4][5].name,"e1112");
    strcpy(maze[4][6].name,"c12");
    strcpy(maze[4][7].name,"e1213");
    strcpy(maze[4][8].name,"c13");
    strcpy(maze[4][9].name,"e1314");
    strcpy(maze[4][10].name,"c14");

    strcpy(maze[6][2].name,"c20");
    strcpy(maze[6][3].name,"e2021");
    strcpy(maze[6][4].name,"c21");
    strcpy(maze[6][5].name,"e2122");
    strcpy(maze[6][6].name,"c22");
    strcpy(maze[6][7].name,"e2223");
    strcpy(maze[6][8].name,"c23");
    strcpy(maze[6][9].name,"e2324");
    strcpy(maze[6][10].name,"c24");

    strcpy(maze[8][2].name,"c30");
    strcpy(maze[8][3].name,"e3031");
    strcpy(maze[8][4].name,"c31");
    strcpy(maze[8][5].name,"e3132");
    strcpy(maze[8][6].name,"c32");
    strcpy(maze[8][7].name,"e3233");
    strcpy(maze[8][8].name,"c33");
    strcpy(maze[8][9].name,"e3334");
    strcpy(maze[8][10].name,"c34");

    strcpy(maze[2][2].name,"c00");
    strcpy(maze[2][3].name,"e0001");
    strcpy(maze[2][4].name,"c01");
    strcpy(maze[2][5].name,"e0102");
    strcpy(maze[2][6].name,"c02");
    strcpy(maze[2][7].name,"e0203");
    strcpy(maze[2][8].name,"c03");
    strcpy(maze[2][9].name,"e0304");
    strcpy(maze[2][10].name,"c04");

    strcpy(maze[3][2].name,"e0010");
    strcpy(maze[3][4].name,"e0111");
    strcpy(maze[3][6].name,"e0212");
    strcpy(maze[3][8].name,"e0313");
    strcpy(maze[3][10].name,"e0414");

    strcpy(maze[5][2].name,"e1020");
    strcpy(maze[5][4].name,"e1121");
    strcpy(maze[5][6].name,"e1222");
    strcpy(maze[5][8].name,"e1323");
    strcpy(maze[5][10].name,"e1424");

    strcpy(maze[7][2].name,"e2030");
    strcpy(maze[7][4].name,"e2131");
    strcpy(maze[7][6].name,"e2232");
    strcpy(maze[7][8].name,"e2333");
    strcpy(maze[7][10].name,"e2434");

    strcpy(maze[9][2].name,"e3040");
    strcpy(maze[9][4].name,"e3141");
    strcpy(maze[9][6].name,"e3242");
    strcpy(maze[9][8].name,"e3343");
    strcpy(maze[9][10].name,"e3444");

    strcpy(maze[10][2].name,"c40");
    strcpy(maze[10][3].name,"e4041");
    strcpy(maze[10][4].name,"c41");
    strcpy(maze[10][5].name,"e4142");
    strcpy(maze[10][6].name,"c42");
    strcpy(maze[10][7].name,"e4243");
    strcpy(maze[10][8].name,"c43");
    strcpy(maze[10][9].name,"e4344");
    strcpy(maze[10][10].name,"c44");
}


void clearmap(Cell maze[13][13])
{
    int i,j;
    for (i = 0; i < 13; i++)
    {
        for (j = 0; j < 13; j++)
        {
            if (maze[i][j].v > 0)
            {
                maze[i][j].v = 0;
            }
        }
    }
}


void print_maze(Cell maze[13][13], int print_mode)
{
    int i,j;

    if(print_mode == PRINT_VALUES)
    {
        for(i = 0; i < 13; i++)
        {
            for(j = 0; j < 13; j++)
            {
                printf("%d\t", maze[i][j].v); /* Double tab for xcode */
            }

            printf("\n\n");
        }
    }
    else if(print_mode == PRINT_VALUES)
    {
        printf(("\n\t\t\t"));
        for (i=0 ; i<13 ; ++i)
        {
            for (j=0 ; j<13; ++j)
                printf("%2d ",maze[i][j].v);
            printf("\n\t\t\t");
        }

    }
}


/* Doet +1 bij alle vakken om zich heen */
void pluseen(int c, int i, int j)
{
    /* Naar boven */
    if (i != 0 && maze[i-1][j].v == 0)
        maze[i-1][j].v = (c+1);
    /* Naar Onder */
    if (i != 12 && maze[i+1][j].v == 0)
        maze[i+1][j].v = (c+1);

    /* Naar links */
    if (j != 0 && maze[i][j-1].v == 0)
        maze[i][j-1].v = (c+1);
    /* Naar rechts */
    if (j != 12 && maze[i][j+1].v == 0)
        maze[i][j+1].v = (c+1);
}


/* Returnt het laagst aanliggende coordinaat */
Location findlowest(Location current)
{
    Location laagst;
    laagst = current;

    /*Check of niet buiten maze valt*/
    /*Check of vak niet gelijk is aan -1*/
    /*Check of vak kleiner is dan vak waar we nu zitten*/

    /* Naar boven */
    if ((current.y-1) >= 0 && maze[current.y-1][current.x].v >= 1  && maze[current.y-1][current.x].v < maze[current.y][current.x].v)
        laagst.y--;
    /* Naar onder */
    else if ((current.y+1) <= 12  && maze[current.y+1][current.x].v >= 1 && maze[current.y+1][current.x].v < maze[current.y][current.x].v)
        laagst.y++;
    /* Naar links */
    else if ((current.x-1) >= 0 && maze[current.y][current.x-1].v >= 1 && maze[current.y][current.x-1].v < maze[current.y][current.x].v)
        laagst.x--;
    /* Naar rechts */
    else if ((current.x+1) <= 12 && maze[current.y][current.x+1].v >= 1 && maze[current.y][current.x+1].v < maze[current.y][current.x].v)
        laagst.x++;
    else
    {
        printf("Geen lowest voor x:%d, y:%d\n", current.x, current.y);
        laagst.x = -1;
        laagst.y = -1;
    }

    return laagst;
}


/* Maakt van een roadnaam een coordinaat in het veld */
/* bv e2030 -> 0,6 : 0,8 */
Location roadnametolocation(char roadstring[8])
{
    Location knpt1, knpt2, road;

    knpt1.y = 2 + ( 2 * ((int)roadstring[1] - 48) );
    knpt1.x = 2 + ( 2 * ((int)roadstring[2] - 48) );

    knpt2.y = 2 + ( 2 * ((int)roadstring[3] - 48) );
    knpt2.x = 2 + ( 2 * ((int)roadstring[4] - 48) );

    road.x = knpt1.x;
    road.y = knpt1.y;

    /* welke kant gaat weg op vanaf knooppunt */
    /*Onder*/
    if (knpt1.y < knpt2.y)
        road.y = knpt1.y + 1;
    /*Boven*/
    if (knpt1.y > knpt2.y)
        road.y = knpt1.y - 1;
    /*Rechts*/
    if (knpt1.x < knpt2.x)
        road.x = knpt1.x + 1;
    /*Links*/
    if (knpt1.x > knpt2.x)
        road.x = knpt1.x - 1;

    return road;
}


/* Vind coordinaat van een punt met gegeven naam [1-12]*/
Location findpunt(int puntnaam)
{
    Location punt;

    if (puntnaam == 1) {punt.x = 4; punt.y = 11;}
    else if (puntnaam == 2) {punt.x = 6; punt.y = 11;}
    else if (puntnaam == 3) {punt.x = 8; punt.y = 11;}
    else if (puntnaam == 4) {punt.x = 11; punt.y = 8;}
    else if (puntnaam == 5) {punt.x = 11; punt.y = 6;}
    else if (puntnaam == 6) {punt.x = 11; punt.y = 4;}
    else if (puntnaam == 7) {punt.y = 1; punt.x = 8;}
    else if (puntnaam == 8) {punt.y = 1; punt.x = 6;}
    else if (puntnaam == 9) {punt.y = 1; punt.x = 4;}
    else if (puntnaam == 10) {punt.y = 4; punt.x = 1;}
    else if (puntnaam == 11) {punt.y = 6; punt.x = 1;}
    else if (puntnaam == 12) {punt.y = 8; punt.x = 1;}
    else
    {
        printf("Foute locatie, is niet bestaand\n");
        exit(1);
    }

    return punt;
}


void leefill(Location source, Location destination)
{
    int c = 1;
    int i, j;

    printf("Leefilling from x:%d, y:%d\n", source.x, source.y);

    maze[destination.y][destination.x].v = 1;

/*    print_maze(maze, PRINT_VALUES);
    printf("---------------------------------------------------------------\n");*/

    /* While niet aangekomen, meer getallen invullen */
    while (maze[source.y][source.x].v == 0)
    {
        /* Voor elke c op de maze , omringende 0 waardes c+1 maken */
        for (i = 0; i < 13; i++)
        {
            for (j = 0; j < 13; j++)
            {
                if (maze[i][j].v == c)
                {
                    pluseen(c, i, j);
                }
            }
        }

        c++;
    }
}

void placeroadblock(Location road)
{
    maze[road.y][road.x].v = -10;
}
