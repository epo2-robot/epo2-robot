#ifndef SERIAL_H
#define SERIAL_H

#include <stdio.h>
#include <string.h>


/* 
Signalen die van robot komen 
*/
#define MINEFOUND 109
#define NEXT_CMD_REQUEST 107

#define NACK 120

/* 
Signalen die naar robot kunnen worden gestuurd 
*/
#define CMD_LEFT 108
#define CMD_RIGHT 114
#define CMD_FORWARD 102
#define CMD_U_TURN 117

#define CMD_HALT 115
#define CMD_START 98


/* Char containing last sent command, used for resending when robot
	has not received correct command. */
char lastsentcommand;

/* Initialiseren seriele communicatie */
void serial_init();

/* Stuur enkele byte */
void serial_write_byte(char bytetowrite);

/* lees enkele byte */
void serial_read_byte(char *buffRead);

/* sluit seriele verbinding */
void serial_close();


#endif /* SERIAL_H */
